var map = null;
var nodesArray = [];
var marker;
  nodesArray.push({id:1,name:'Node 1',lat:21.04930883624107,lng:79.03445899486542,icon:'http://maps.google.com/mapfiles/ms/icons/green-dot.png'},
  	{id:2,name:'Node 2',lat:21.048903312480057,lng:79.03571963310242,icon:'http://maps.google.com/mapfiles/ms/icons/red-dot.png'},
  	{id:3,name:'Node 3',lat:21.048347592939514,lng:79.03576254844666,icon:'http://maps.google.com/mapfiles/ms/icons/orange-dot.png'},
  	{id:4,name:'Node 4',lat:21.048497787614483,lng:79.03430342674255,icon:'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'}); 
  
function showlocation() {
               // One-shot position request.
                navigator.geolocation.getCurrentPosition(callback);
}            
function initNode() {
            
	getAllNodeStatus().then(function(result){
				console.log(result);
				for(var i=0;i<result.length;i++){
					if(result[i].status==="Low"){
						nodesArray[i].status = "Low";						
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
					}
					if(result[i].status==="Busy"){
						nodesArray[i].status = "Busy";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png';	
					}
					if(result[i].status==="Emergency"){
						nodesArray[i].status = "Emergency";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';	
					}
					if(result[i].status==="Normal"){
						nodesArray[i].status = "Normal";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';	
					}
				}

			var position = {};
			position.coords = {};
            position.coords.latitude = nodesArray[0].lat;
			position.coords.longitude = nodesArray[0].lng;
			position.coords.icon = nodesArray[0].icon;
			position.coords.id = nodesArray[0].id;
			position.coords.status = nodesArray[0].status;
			callback(position);

			},function(error){
				console.log(error);
			});

            
}          
function callback(position) {         
        var lat = position.coords.latitude.toString();
        var lon = position.coords.longitude.toString();      
         
        document.getElementById('latitude').innerHTML = lat;
        document.getElementById('longitude').innerHTML = lon;
             
        var latLong = new google.maps.LatLng(lat, lon);
		if (marker) {
        	//if marker already was created change positon
        	marker.setPosition(latLong);
        	marker.setIcon(position.coords.icon);        	
	    } else {
	        //create a marker
	        marker = new google.maps.Marker({
	                    position: latLong
	                }); 
	        marker.setIcon(nodesArray[0].icon);
	        getNodeDetails(nodesArray[0].id);
	    }
 		marker.setMap(map);
        map.setZoom(18);
        map.setCenter(marker.getPosition());
        google.maps.event.addListener(map, "click", function (e) {

    	//lat and lng is available in e object
    	var latLng = e.latLng;
    	var obj = JSON.stringify(latLng);
    	var parsedObj = JSON.parse(obj);
    	console.log(parsedObj);
		var latLong = new google.maps.LatLng(parsedObj.lat, parsedObj.lng);
                var marker = new google.maps.Marker({
                    position: latLong
                });                       
        marker.setMap(map);
        map.setZoom(18);
        map.setCenter(marker.getPosition());
        document.getElementById('latitude').innerHTML = parsedObj.lat;
        document.getElementById('longitude').innerHTML = parsedObj.lng;
		});
}      
       
google.maps.event.addDomListener(window, 'load', initMap);

      function initMap() {
        var mapOptions = {
          center: new google.maps.LatLng(0, 0),
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.SATELLITE
        };
        map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);       
}


function displayNodeLocation(value){	
	value = parseInt(value);
	getNodeDetails(value);
	var position = {};
	position.coords = {};
	for (var i = 0; i < nodesArray.length; i++) {
		if(nodesArray[i].id==value){
			position.coords.id = nodesArray[i].id;
			position.coords.latitude = nodesArray[i].lat;
			position.coords.longitude = nodesArray[i].lng;
			position.coords.icon = nodesArray[i].icon;
			position.coords.status = nodesArray[i].status;
			callback(position);
		}
	};
}
function getNodeDetails(nodeId) {	
	var id = parseInt(nodeId);
	getNodeById(id).then(function(result){
		console.log(result);
		$('#traficDetails').html(result.detail);		
		$('#formGroupInputSmall').val(result.status);
	},function(error){
		console.log(error);
	});
};

function getNodeStatus(){
	getAllNodeStatus().then(function(result){
				console.log(result);
				for(var i=0;i<result.length;i++){
					if(result[i].status==="Low"){
						nodesArray[i].status = "Low";						
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
					}
					if(result[i].status==="Busy"){
						nodesArray[i].status = "Busy";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png';	
					}
					if(result[i].status==="Emergency"){
						nodesArray[i].status = "Emergency";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';	
					}
					if(result[i].status==="Normal"){
						nodesArray[i].status = "Normal";
						nodesArray[i].icon = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';	
					}
				}
			},function(error){
				console.log(error);
			});
}
window.onload = initNode;